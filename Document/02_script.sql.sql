-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2016 at 09:28 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_qlqa`
--

-- --------------------------------------------------------

--
-- Table structure for table `ban_an`
--

CREATE TABLE `ban_an` (
  `id` int(11) NOT NULL,
  `songuoitoida` int(11) DEFAULT NULL,
  `tinhtrang` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ban_an`
--

INSERT INTO `ban_an` (`id`, `songuoitoida`, `tinhtrang`) VALUES
(1, 10, 'yes'),
(2, 10, 'yes'),
(3, 10, 'yes'),
(4, 10, 'yes'),
(5, 10, 'yes'),
(6, 10, 'yes'),
(7, 10, 'yes'),
(8, 10, 'yes'),
(9, 10, 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `calamviec`
--

CREATE TABLE `calamviec` (
  `id` int(11) NOT NULL,
  `calamviec` enum('sang','chieu','toi') DEFAULT NULL,
  `ngaylamviec` int(11) DEFAULT NULL,
  `tinhtrang` varchar(45) DEFAULT NULL,
  `nhanvienxinphepvang` varchar(45) DEFAULT NULL,
  `nhanvienditre` varchar(45) DEFAULT NULL,
  `chamcong` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `chitiet_hoadon`
--

CREATE TABLE `chitiet_hoadon` (
  `id` int(11) NOT NULL,
  `tenmonan` varchar(45) DEFAULT NULL,
  `soluong` int(11) DEFAULT NULL,
  `gia` float DEFAULT NULL,
  `ngaythang` datetime DEFAULT NULL,
  `ghichu` varchar(45) DEFAULT NULL,
  `idmon` int(11) NOT NULL,
  `idban` int(11) NOT NULL,
  `idhoadon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hoadon`
--

CREATE TABLE `hoadon` (
  `id` int(11) NOT NULL,
  `ngaythang` datetime DEFAULT NULL,
  `tongtien` float DEFAULT NULL,
  `id_ban` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mon_an`
--

CREATE TABLE `mon_an` (
  `id` int(11) NOT NULL,
  `tenmonan` varchar(50) NOT NULL,
  `nguyenlieu` varchar(50) NOT NULL,
  `gia` float NOT NULL,
  `tinhtrang` varchar(50) NOT NULL,
  `trangthainguyenlieu` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mon_an`
--

INSERT INTO `mon_an` (`id`, `tenmonan`, `nguyenlieu`, `gia`, `tinhtrang`, `trangthainguyenlieu`) VALUES
(1, 'ewq2', 'q', 50, 'yes', 'no'),
(2, 'canh chua', 'ca', 1000, 'no', 'no'),
(3, 'ca kho ', 'ca', 50000, 'no', 'yes'),
(4, 'ca', 'vs', 500000, 'yes', 'yes'),
(5, 'canh cai', 'dad', 1000, 'no', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `nhanvien`
--

CREATE TABLE `nhanvien` (
  `id` int(11) NOT NULL,
  `hoten` varchar(45) DEFAULT NULL,
  `giotinh` varchar(45) DEFAULT NULL,
  `ngaysinh` datetime DEFAULT NULL,
  `dienthoai` int(11) DEFAULT NULL,
  `diachi` varchar(45) DEFAULT NULL,
  `cmnd` int(11) DEFAULT NULL,
  `thoigianbatdau` datetime DEFAULT NULL,
  `trinhdovitinh` varchar(45) DEFAULT NULL,
  `calamviec` varchar(45) DEFAULT NULL,
  `vitri` varchar(45) DEFAULT NULL,
  `nghiviec` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nhanvien`
--

INSERT INTO `nhanvien` (`id`, `hoten`, `giotinh`, `ngaysinh`, `dienthoai`, `diachi`, `cmnd`, `thoigianbatdau`, `trinhdovitinh`, `calamviec`, `vitri`, `nghiviec`) VALUES
(1, 'Huynh Tam', 'nam', '1991-03-07 00:00:00', 1224925150, 'hcm', 3221413, '2016-03-09 00:00:00', 'van phong', 'sang', 'nhan vien', 'chua'),
(2, 'linh nguyen', 'Nam', NULL, 1224925150, 'hcm', 3221413, NULL, 'Word', 'Sang', 'Phuc Vu', '0'),
(3, 'Quan song', 'Nam', NULL, 1224925150, 'hcm', 3221413, NULL, 'Word', 'Sang', 'Phuc Vu', '0');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(11) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `pass` varchar(45) DEFAULT NULL,
  `user_role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `email`, `pass`, `user_role`) VALUES
('user', 'infinitylog01@gmail.com', '123456', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ban_an`
--
ALTER TABLE `ban_an`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `calamviec`
--
ALTER TABLE `calamviec`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chitiet_hoadon`
--
ALTER TABLE `chitiet_hoadon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hoadon`
--
ALTER TABLE `hoadon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mon_an`
--
ALTER TABLE `mon_an`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nhanvien`
--
ALTER TABLE `nhanvien`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chitiet_hoadon`
--
ALTER TABLE `chitiet_hoadon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;
--
-- AUTO_INCREMENT for table `hoadon`
--
ALTER TABLE `hoadon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
