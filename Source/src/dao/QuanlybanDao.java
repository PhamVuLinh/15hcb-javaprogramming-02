/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import GUI.JlFrameQuanLyBan;
import entity.BanAn;
import entity.MonAn;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

/**
 *
 * @author Tamkhung
 */
public class QuanlybanDao {
    private int idban=1;
    private int ban_chon;

    public void setBan_chon(int ban_chon) {
        this.ban_chon = ban_chon;
    }
    public void setId(int id) {
        this.idban = idban;
    }
    public int getId() {
        return idban;
    }
    public int getBan_chon() {
        return ban_chon;
    }
    
    
    
    private SessionFactory sf = null;
     List<BanAn> list = new ArrayList<BanAn>();
     
    private float tongban = 0;
    public void setTongban(float tongban) {
        this.tongban = tongban;
    }
    public float getTongban() {
        return tongban;
    }
   
        Map<Integer,Vector> myMap = new HashMap<>();
        Vector ban=new Vector();
        Vector mon=new Vector(); 

      public float tinhtien(int choose,DefaultTableModel tableban){
            //JlFrameQuanLyBan qlb=new JlFrameQuanLyBan();
            myMap.put(choose, tableban.getDataVector());
                ban = myMap.get(choose);
                 for(int i=0;i<ban.size();i++)
                 {   
                    mon=(Vector) ban.get(i);
                    //System.out.print(ban_mon.get(3));
                    try{
                    tongban=tongban + (Float.parseFloat(mon.get(2).toString()) * (float)mon.get(3));
                   // System.out.print(ban_mon.get(2)  + "  and " + ban_mon.get(3));
                    }catch(Exception e){
                    
                    }
                 }
                //System.out.print(tongban);  
     
        return tongban;
  }
  
        
        
    public List<BanAn> finAll(){ 
        try {
            sf = HibernateUtil.getSessionFactory();
            sf.getCurrentSession().beginTransaction();
            list = sf.getCurrentSession().createCriteria(BanAn.class).list();
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        } finally{
           sf.getCurrentSession().close();
            return list;
        }
        
    }
    

    public List<BanAn> CheckBanAnYes (){ 
      List<BanAn> list = new ArrayList<>();
         for (BanAn banan :this.finAll()){
             list.add(banan);
            if(banan.getTinhtrang().equals("yes")){
               list.add(banan);
            }
            else{
            }
        }
        return list;
    } 


    
    public BanAn find(int ten_ban_an){
        BanAn ban_an = new BanAn();
        try {
            sf = HibernateUtil.getSessionFactory();
            sf.getCurrentSession().beginTransaction();
            ban_an = (BanAn) sf.getCurrentSession().get(BanAn.class,ten_ban_an); 
        } catch (Exception e) {
            return null;
        }finally{
            HibernateUtil.closeSessionFactory();
            return ban_an;
        }
    }
    public boolean delete(BanAn ban_an){
        try {
            sf = HibernateUtil.getSessionFactory();
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().delete(ban_an);
            sf.getCurrentSession().getTransaction().commit();
            
            
        } catch (Exception e) {
            sf.getCurrentSession().getTransaction().rollback();
            return false;
        }finally{
            HibernateUtil.closeSessionFactory();
            return true;
        }
    }
    public boolean saveorupdate(BanAn ban_an){
        try {
            sf = HibernateUtil.getSessionFactory();
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().saveOrUpdate(ban_an);
            sf.getCurrentSession().getTransaction().commit();
            
            
        } catch (Exception e) {
            return false;
        }finally{
            HibernateUtil.closeSessionFactory();
            return true;
        }
   
    }
    public boolean query(BanAn ban_an){
        try {
            sf= HibernateUtil.getSessionFactory();
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().createQuery(null);
            
        } catch (Exception e) {
            return false;
        }finally{
             HibernateUtil.closeSessionFactory();
             return true;
        }
     
    }

    
    
    
    public Object danhsach() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean delete(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean saveorupdate(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
