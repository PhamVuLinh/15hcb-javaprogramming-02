/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;
import java.util.*;
import entity.*;
import org.hibernate.*;
import util.HibernateUtil;



public class SanPhamDao {
    private SessionFactory sf = null;
    public List<MonAn> finAll(){
        List<MonAn> list = new ArrayList<MonAn>();
        try {
            sf = HibernateUtil.getSessionFactory();
            sf.getCurrentSession().beginTransaction();
            list = sf.getCurrentSession().createCriteria(MonAn.class).list(); 
            
        } catch (Exception e) {
            return null;
        } finally{
            sf.getCurrentSession().close();
            return list;
        }
        
    }
    
   

      public List<MonAn> CheckMonAn (){ 
      List<MonAn> list = new ArrayList<>();
         for (MonAn monan :this.finAll()){
             //System.out.print(banan.getTinhtrang());
            if(monan.getTinhtrang().equals("yes")){
              //  System.out.print(" con "); 
               list.add(monan);
            }
            else{
           // System.out.print(" het ");
            }
        }
        return list;
    } 
    
    public MonAn find(String tensanpham){
        MonAn mon_an = new MonAn();
        try {
            sf = HibernateUtil.getSessionFactory();
            sf.getCurrentSession().beginTransaction();
            mon_an =  (MonAn) sf.getCurrentSession().get(MonAn.class,Integer.parseInt(tensanpham));
        } catch (Exception e) {
            return null;
        }finally{
            HibernateUtil.closeSessionFactory();
            return mon_an;
        }
    }

    
    
    public boolean delete(MonAn mon_an){
        try {
            sf = HibernateUtil.getSessionFactory();
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().delete(mon_an);
            sf.getCurrentSession().getTransaction().commit();
            
            
        } catch (Exception e) {
            sf.getCurrentSession().getTransaction().rollback();
            return false;
        }finally{
            HibernateUtil.closeSessionFactory();
            return true;
        }
    }
     public boolean delete(ChitietHoadon cthd){
        try {
            sf = HibernateUtil.getSessionFactory();
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().delete(cthd);
            sf.getCurrentSession().getTransaction().commit();
            
            
        } catch (Exception e) {
            sf.getCurrentSession().getTransaction().rollback();
            return false;
        }finally{
            HibernateUtil.closeSessionFactory();
            return true;
        }
    }
      
     public boolean delete(Hoadon hd){
        try {
            sf = HibernateUtil.getSessionFactory();
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().delete(hd);
            sf.getCurrentSession().getTransaction().commit();
            
            
        } catch (Exception e) {
            sf.getCurrentSession().getTransaction().rollback();
            return false;
        }finally{
            HibernateUtil.closeSessionFactory();
            return true;
        }
    }
     public ChitietHoadon findCthd(String tensanpham){
        ChitietHoadon cthd = new ChitietHoadon();
        try {
            sf = HibernateUtil.getSessionFactory();
            sf.getCurrentSession().beginTransaction();
            cthd =  (ChitietHoadon) sf.getCurrentSession().get(ChitietHoadon.class,Integer.parseInt(tensanpham));
        } catch (Exception e) {
            return null;
        }finally{
            HibernateUtil.closeSessionFactory();
            return cthd;
        }
    }
    
    
    public boolean saveorupdate(MonAn mon_an){
        try {
            sf = HibernateUtil.getSessionFactory();
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().saveOrUpdate(mon_an);
            sf.getCurrentSession().getTransaction().commit();  
            
        } catch (Exception e) {
            return false;
        }finally{
            HibernateUtil.closeSessionFactory();
            return true;
        }
    }
    public boolean saveorupdate(ChitietHoadon cthd){
        boolean flag=false;
        try {
            sf = HibernateUtil.getSessionFactory();
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().save(cthd);
            sf.getCurrentSession().getTransaction().commit();
            flag=true;
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            HibernateUtil.closeSessionFactory();
            return flag;
        }
    }
    public boolean saveorupdate(Hoadon hoadon){
        try {
            sf = HibernateUtil.getSessionFactory();
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().save(hoadon);
            sf.getCurrentSession().getTransaction().commit();  
            
        } catch (Exception e) {
            return false;
        }finally{
            HibernateUtil.closeSessionFactory();
            return true;
        }
    }
      public boolean saveorupdateHoadon(Hoadon hoadon){
        try {
            sf = HibernateUtil.getSessionFactory();
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().saveOrUpdate(hoadon);
            sf.getCurrentSession().getTransaction().commit();  
            
        } catch (Exception e) {
            return false;
        }finally{
            HibernateUtil.closeSessionFactory();
            return true;
        }
    }
    
    
    
    public boolean query(MonAn mon_an){
        try {
            sf= HibernateUtil.getSessionFactory();
            sf.getCurrentSession().beginTransaction();
            sf.getCurrentSession().createQuery(null);
            
        } catch (Exception e) {
            return false;
        }finally{
             HibernateUtil.closeSessionFactory();
             return true;
        }
     
    }
   
    public List query(String query){
        List results;
        try {
            
            sf= HibernateUtil.getSessionFactory();
            sf.getCurrentSession().beginTransaction();
            //sf.getCurrentSession().createQuery(querry);
            Query qr=sf.getCurrentSession().createQuery(query);
            results=qr.list(); 
        } catch (Exception e) {
            return null;
        }finally{
             HibernateUtil.closeSessionFactory();
             
        }
        return results;
     
    }

    
}
