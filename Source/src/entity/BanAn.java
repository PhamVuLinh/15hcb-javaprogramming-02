package entity;
// Generated 08-Apr-2016 00:14:17 by Hibernate Tools 4.3.1


import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="ban_an"
    ,catalog="db_qlqa"
)
public class BanAn  implements java.io.Serializable { 

List <MonAn> Listmon=new ArrayList();

    public void setListmon(List<MonAn> Listmon) {
        this.Listmon = Listmon;
    }

    public List<MonAn> getListmon() {
        return Listmon;
    }
     private int id;
     private Integer songuoitoida;
     private String tinhtrang;

    public BanAn() {
    }
     public float tinhTien(){
        float tongtien=0;
        for(MonAn monan: Listmon){
        tongtien+= monan.getGia()*monan.getSoluong();
        }
        return tongtien;
    }
	
    public BanAn(int id) {
        this.id = id;
    }
    public BanAn(int id, Integer songuoitoida, String tinhtrang) {
       this.id = id;
       this.songuoitoida = songuoitoida;
       this.tinhtrang = tinhtrang;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public Integer getSonguoitoida() {
        return this.songuoitoida;
    }
    
    public void setSonguoitoida(Integer songuoitoida) {
        this.songuoitoida = songuoitoida;
    }
    public String getTinhtrang() {
        return this.tinhtrang;
    }
    
    public void setTinhtrang(String tinhtrang) {
        this.tinhtrang = tinhtrang;
    }
    


}


