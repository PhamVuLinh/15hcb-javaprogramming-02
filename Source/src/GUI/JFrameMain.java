/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;
import entity.*;
import dao.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JToolBar;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;

/**
 *
 * @author My PC
 */
public class JFrameMain extends javax.swing.JFrame {
    
    private static JFrameMain instance;
    public JInternalFrame currentFrame;
   /// QuanlybanDao   banandao =new QuanlybanDao();
   // List<BanAn> danhSachBanAn=new  ArrayList<BanAn>();;
   
     
    /**
     * Creates new form JFrameMain
     */
    public JFrameMain() {
        instance = this;
        initComponents();        
    }
    
    public static JFrameMain getInstance(){
        return instance;
    }

    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar3 = new javax.swing.JToolBar();
        jButton6 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jToolBar4 = new javax.swing.JToolBar();
        jButton8 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jToolBar1 = new javax.swing.JToolBar();
        jButton11 = new javax.swing.JButton();
        jBntMonan = new javax.swing.JButton();
        jToolBar5 = new javax.swing.JToolBar();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(204, 204, 204));
        setMinimumSize(new java.awt.Dimension(1300, 700));
        getContentPane().setLayout(null);

        jToolBar3.setFloatable(false);
        jToolBar3.setRollover(true);

        jButton6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/box1.png"))); // NOI18N
        jButton6.setText("Thống kê");
        jButton6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton6.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar3.add(jButton6);

        jButton3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/quan ly nv  1.png"))); // NOI18N
        jButton3.setText("Quản lý nhân viên");
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar3.add(jButton3);

        getContentPane().add(jToolBar3);
        jToolBar3.setBounds(880, 0, 200, 90);

        jToolBar4.setFloatable(false);
        jToolBar4.setRollover(true);

        jButton8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/doi mat khau 1.png"))); // NOI18N
        jButton8.setText("Đổi mật khẩu");
        jButton8.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton8.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jToolBar4.add(jButton8);

        jButton7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/exit.png"))); // NOI18N
        jButton7.setText("Thoát");
        jButton7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton7.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jToolBar4.add(jButton7);

        getContentPane().add(jToolBar4);
        jToolBar4.setBounds(1100, 0, 150, 90);

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        jButton11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/cai ban.png"))); // NOI18N
        jButton11.setText("Quản lý bàn ăn");
        jButton11.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton11.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton11);

        jBntMonan.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jBntMonan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/food.png"))); // NOI18N
        jBntMonan.setText("Quản lý món ăn");
        jBntMonan.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBntMonan.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBntMonan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBntMonanActionPerformed(evt);
            }
        });
        jToolBar1.add(jBntMonan);

        getContentPane().add(jToolBar1);
        jToolBar1.setBounds(310, 0, 240, 90);

        jToolBar5.setFloatable(false);
        jToolBar5.setRollover(true);
        jToolBar5.setBorderPainted(false);
        jToolBar5.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        getContentPane().add(jToolBar5);
        jToolBar5.setBounds(50, 90, 1200, 520);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/stock-photo-healthy-food-background-studio-photography-of-different-fruits-and-vegetables-on-wooden-table-210862876.jpg"))); // NOI18N
        getContentPane().add(jLabel3);
        jLabel3.setBounds(-20, 0, 1500, 700);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBntMonanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBntMonanActionPerformed
        
        JlFrameMonAn jf = new JlFrameMonAn();
        
        jf.setVisible(true);

       // jDesktopPane1.ad;
       jToolBar5.add(jf);
       
    }//GEN-LAST:event_jBntMonanActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        JlFrameQuanLyNhanVien jf = new JlFrameQuanLyNhanVien();
        jf.setVisible(true);
        jToolBar5.add(jf);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
        JlFrameDoiMatKhau jf=new JlFrameDoiMatKhau();
        jf.setVisible(true);
        jToolBar5.add(jf);
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
         System.exit(0);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        JlFrameQuanLyBan jf = new JlFrameQuanLyBan();
        jf.setVisible(true);
        jToolBar5.add(jf);        
        jf.loadmonan();
        jf.loadbackroundButton();    
    }//GEN-LAST:event_jButton11ActionPerformed

    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrameMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrameMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrameMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrameMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
               // new JFrameDangnhap().setVisible(true);
                JFrameMain main=new JFrameMain();
                main.setVisible(true);
                //JlFrameQuanLyBan qlb=new JlFrameQuanLyBan();
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBntMonan;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar3;
    private javax.swing.JToolBar jToolBar4;
    private javax.swing.JToolBar jToolBar5;
    // End of variables declaration//GEN-END:variables
}
