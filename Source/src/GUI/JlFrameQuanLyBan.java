/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import dao.Inhoadon;
import dao.QuanlybanDao;
import dao.SanPhamDao;
import entity.BanAn;
import entity.ChitietHoadon;
import entity.Hoadon;
import entity.MonAn;
import java.awt.Color;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.table.DefaultTableModel;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Tamkhung
 */
public class JlFrameQuanLyBan extends javax.swing.JInternalFrame {
   
    QuanlybanDao banandao=new QuanlybanDao();
    SanPhamDao sanphamdao=new SanPhamDao();
    BanAn banan;
    MonAn monan;
    Hoadon hoadon;
    ChitietHoadon cthd;
    Map<Integer,BanAn> dsban = new HashMap<>();
    int chooseTT;
    public void setDsban(Map<Integer, BanAn> dsban) {
        this.dsban = dsban;
    }

    public Map<Integer,BanAn> getDsban() {
        return dsban;
    }
    DefaultTableModel Tablechonmon = new DefaultTableModel();
    DefaultTableModel TableBanMon= new DefaultTableModel();
   
    JInternalFrame instance;

    public float gopban(BanAn ban1,BanAn ban2){
        return ban1.tinhTien()+ban2.tinhTien();

    }

    private int choose =0;
    public void setChoose(int choose) {
        this.choose = choose;
    }

    public int getChoose() {
        return choose;
    }
    private int idhoadon_NewTinhTien;
    private int idhoadon_Saukhithanhtoan;
    
    /**
     * Creates new form JlFrameQuanLyBan
     */
    public JlFrameQuanLyBan() {
        if(banandao.getId()==banandao.getBan_chon())
        {
            System.out.print("Trung ban ");
        }
        else{
            System.out.print(banandao.getId() + " " + banandao.getBan_chon());
        }
        if(JFrameMain.getInstance().currentFrame != null){
            JFrameMain.getInstance().currentFrame.setVisible(false);
            JFrameMain.getInstance().currentFrame.dispose();
        }
        instance = this;
        JFrameMain.getInstance().currentFrame = this;
        initComponents();
        Tablechonmon.addColumn("ID");
        Tablechonmon.addColumn(" Tên Mon an ++++++++ ");
        Tablechonmon.addColumn("So Luong ");
        Tablechonmon.addColumn(" Gia ");
        jTablechonmon.setModel(Tablechonmon);
       /////////////////////////////////
    }

    
public void loadbackroundButton(){
    
    System.out.println("___________________");
     List<BanAn> listban = new ArrayList<>();
        listban= banandao.finAll();
        for(int i=0;i<listban.size();i++)
        {
            if(jButton17.getIconTextGap()==(listban.get(i).getId()) )              
            {
                if(listban.get(i).getTinhtrang().equals("yes"))
                    jButton17.setBackground(Color.white);
                else
                    jButton17.setBackground(Color.GRAY);
                
            }
            if(jButton18.getIconTextGap()==(listban.get(i).getId()) )              
            {
                if(listban.get(i).getTinhtrang().equals("yes"))
                    jButton18.setBackground(Color.white);
                else
                    jButton18.setBackground(Color.GRAY);
            }

            if(jButton19.getIconTextGap()==(listban.get(i).getId()) )              
            {
                if(listban.get(i).getTinhtrang().equals("yes"))
                    jButton19.setBackground(Color.white);
                else
                    jButton19.setBackground(Color.GRAY);
            }
            if(jButton20.getIconTextGap()==(listban.get(i).getId()) )              
            {
                if(listban.get(i).getTinhtrang().equals("yes"))
                    jButton20.setBackground(Color.white);
                else
                    jButton20.setBackground(Color.GRAY);
            }

            if(jButton21.getIconTextGap()==(listban.get(i).getId()) )              
            {
                if(listban.get(i).getTinhtrang().equals("yes"))
                    jButton21.setBackground(Color.white);
                else
                    jButton21.setBackground(Color.GRAY);
            }
             if(jButton22.getIconTextGap()==(listban.get(i).getId()) )              
            {
                if(listban.get(i).getTinhtrang().equals("yes"))
                    jButton22.setBackground(Color.white);
                else
                    jButton22.setBackground(Color.GRAY);
            }

            if(jButton23.getIconTextGap()==(listban.get(i).getId()) )              
            {
                if(listban.get(i).getTinhtrang().equals("yes"))
                    jButton23.setBackground(Color.white);
                else
                    jButton23.setBackground(Color.GRAY);
            }
             if(jButton24.getIconTextGap()==(listban.get(i).getId()) )              
            {
                if(listban.get(i).getTinhtrang().equals("yes"))
                    jButton24.setBackground(Color.white);
                else
                    jButton24.setBackground(Color.GRAY);
            }

            if(jButton25.getIconTextGap()==(listban.get(i).getId()) )              
            {
                if(listban.get(i).getTinhtrang().equals("yes"))
                    jButton25.setBackground(Color.white);
                else
                    jButton25.setBackground(Color.GRAY);
            }
              
        }
}
 
      void loadmonan() {  
         if(choose==0){
        TableBanMon.addColumn("ID");
        TableBanMon.addColumn(" Tên Mon an ++++++++ ");
        TableBanMon.addColumn("Nguyen lieu   ");
        TableBanMon.addColumn(" Gia ");
        TableBanMon.addColumn("Tinh trang");
        TableBanMon.addColumn("Trang thai nguyen lieu");
       
       for (MonAn monan : sanphamdao.finAll()) {
            TableBanMon.addRow(new Object[]{monan.getId(),monan.getTenmonan(),monan.getNguyenlieu(),monan.getGia(), monan.getTinhtrang(),monan.getTrangthainguyenlieu()});
            // System.out.println("monan.getId()");
        }
        jTableBanMon.setModel(TableBanMon);
        jTableBanMon.repaint();
        jTableBanMon.revalidate();  
         }
         
         
         
     }
      

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextBanSo = new javax.swing.JTextField();
        jTextTongTien = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTablechonmon = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jButton17 = new javax.swing.JButton();
        jButton18 = new javax.swing.JButton();
        jButton19 = new javax.swing.JButton();
        jButton20 = new javax.swing.JButton();
        jButton21 = new javax.swing.JButton();
        jButton22 = new javax.swing.JButton();
        jButton23 = new javax.swing.JButton();
        jButton24 = new javax.swing.JButton();
        jButton25 = new javax.swing.JButton();
        jBtnCapNhatBan = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jBtnInhoadon = new javax.swing.JButton();
        jBtnBoQua = new javax.swing.JButton();
        jBtnGopBan = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableBanMon = new javax.swing.JTable();
        jComboBoxSoLuong = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jBtnchuyenban = new javax.swing.JButton();
        jTextIDmon = new javax.swing.JTextField();
        jBtnCapNhatSl = new javax.swing.JButton();

        setClosable(true);
        setPreferredSize(new java.awt.Dimension(1200, 520));
        getContentPane().setLayout(null);

        jLabel1.setText("Ban");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(10, 36, 18, 14);

        jLabel3.setText("Tổng tiền");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(10, 87, 60, 14);

        jTextBanSo.setText("Ban ");
        jTextBanSo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextBanSoActionPerformed(evt);
            }
        });
        getContentPane().add(jTextBanSo);
        jTextBanSo.setBounds(59, 20, 90, 30);

        jTextTongTien.setText("vnd");
        jTextTongTien.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextTongTienActionPerformed(evt);
            }
        });
        getContentPane().add(jTextTongTien);
        jTextTongTien.setBounds(60, 70, 90, 30);

        jTablechonmon.setAutoCreateRowSorter(true);
        jTablechonmon.setBackground(new java.awt.Color(204, 204, 204));
        jTablechonmon.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jTablechonmon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTablechonmonMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTablechonmon);

        getContentPane().add(jScrollPane2);
        jScrollPane2.setBounds(24, 127, 300, 121);

        jLabel5.setText("Danh sach ban");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(10, 266, 71, 14);

        jButton17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/cai ban.png"))); // NOI18N
        jButton17.setText("B1");
        jButton17.setIconTextGap(1);
        jButton17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton17ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton17);
        jButton17.setBounds(10, 286, 97, 57);

        jButton18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/cai ban.png"))); // NOI18N
        jButton18.setText("B2");
        jButton18.setIconTextGap(2);
        jButton18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton18ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton18);
        jButton18.setBounds(113, 286, 97, 57);

        jButton19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/cai ban.png"))); // NOI18N
        jButton19.setText("B3");
        jButton19.setIconTextGap(3);
        jButton19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton19ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton19);
        jButton19.setBounds(216, 286, 100, 57);

        jButton20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/cai ban.png"))); // NOI18N
        jButton20.setText("B4");
        jButton20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton20ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton20);
        jButton20.setBounds(10, 349, 97, 57);

        jButton21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/cai ban.png"))); // NOI18N
        jButton21.setText("B5");
        jButton21.setIconTextGap(5);
        jButton21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton21ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton21);
        jButton21.setBounds(113, 349, 97, 57);

        jButton22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/cai ban.png"))); // NOI18N
        jButton22.setText("B6");
        jButton22.setIconTextGap(6);
        jButton22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton22ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton22);
        jButton22.setBounds(214, 349, 100, 57);

        jButton23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/cai ban.png"))); // NOI18N
        jButton23.setText("B7");
        jButton23.setIconTextGap(7);
        jButton23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton23ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton23);
        jButton23.setBounds(6, 410, 99, 57);

        jButton24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/cai ban.png"))); // NOI18N
        jButton24.setText("B8");
        jButton24.setAutoscrolls(true);
        jButton24.setIconTextGap(8);
        jButton24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton24ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton24);
        jButton24.setBounds(110, 410, 101, 57);

        jButton25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/cai ban.png"))); // NOI18N
        jButton25.setText("B9");
        jButton25.setIconTextGap(9);
        jButton25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton25ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton25);
        jButton25.setBounds(214, 410, 100, 57);

        jBtnCapNhatBan.setText("Cập Nhật Bàn");
        jBtnCapNhatBan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jBtnCapNhatBanMouseClicked(evt);
            }
        });
        jBtnCapNhatBan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnCapNhatBanActionPerformed(evt);
            }
        });
        getContentPane().add(jBtnCapNhatBan);
        jBtnCapNhatBan.setBounds(350, 430, 110, 40);

        jButton2.setText("Thanh Toán");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2);
        jButton2.setBounds(870, 430, 110, 40);

        jBtnInhoadon.setText("In Hóa Đơn");
        jBtnInhoadon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnInhoadonActionPerformed(evt);
            }
        });
        getContentPane().add(jBtnInhoadon);
        jBtnInhoadon.setBounds(985, 430, 100, 40);

        jBtnBoQua.setText("Bỏ Qua");
        jBtnBoQua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnBoQuaActionPerformed(evt);
            }
        });
        getContentPane().add(jBtnBoQua);
        jBtnBoQua.setBounds(680, 440, 90, 30);

        jBtnGopBan.setText("Gộp Bàn");
        jBtnGopBan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnGopBanActionPerformed(evt);
            }
        });
        getContentPane().add(jBtnGopBan);
        jBtnGopBan.setBounds(480, 440, 90, 30);

        jTableBanMon.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jTableBanMon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableBanMonMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableBanMon);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(348, 0, 750, 410);

        jComboBoxSoLuong.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "" }));
        jComboBoxSoLuong.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jComboBoxSoLuongMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jComboBoxSoLuongMousePressed(evt);
            }
        });
        jComboBoxSoLuong.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxSoLuongActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBoxSoLuong);
        jComboBoxSoLuong.setBounds(180, 70, 50, 30);

        jLabel4.setText("ID Mon");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(180, 24, 40, 30);

        jBtnchuyenban.setText("Chuyển bàn");
        jBtnchuyenban.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jBtnchuyenbanMouseExited(evt);
            }
        });
        jBtnchuyenban.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnchuyenbanActionPerformed(evt);
            }
        });
        getContentPane().add(jBtnchuyenban);
        jBtnchuyenban.setBounds(570, 440, 110, 30);
        getContentPane().add(jTextIDmon);
        jTextIDmon.setBounds(230, 20, 100, 30);

        jBtnCapNhatSl.setText("Cap Nhật SL");
        jBtnCapNhatSl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnCapNhatSlActionPerformed(evt);
            }
        });
        getContentPane().add(jBtnCapNhatSl);
        jBtnCapNhatSl.setBounds(230, 70, 100, 30);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextBanSoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextBanSoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextBanSoActionPerformed

    private void jTextTongTienActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextTongTienActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextTongTienActionPerformed

    private void jButton25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton25ActionPerformed
        // TODO add your handling code here:    
      this.setChoose(9);
   
      jTextBanSo.setText("B9");
    }//GEN-LAST:event_jButton25ActionPerformed

    
    
    private void jButton17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton17ActionPerformed
        this.setChoose(1);
        Loadban_Button();
       jTextBanSo.setText("B1");
        jTablechonmon.removeAll();
        
    }//GEN-LAST:event_jButton17ActionPerformed

    private void jButton18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton18ActionPerformed
        // TODO add your handling code here:
        this.setChoose(2);
        Loadban_Button();
        jTextBanSo.setText("B2");
    }//GEN-LAST:event_jButton18ActionPerformed

    private void jButton24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton24ActionPerformed
        // TODO add your handling code here: 
        this.setChoose(8);
        Loadban_Button();
    jTextBanSo.setText("B8");
    }//GEN-LAST:event_jButton24ActionPerformed

    private void jButton19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton19ActionPerformed
        // TODO add your handling code here:
        this.setChoose(3);
        Loadban_Button();
        jTextBanSo.setText("B3");
    }//GEN-LAST:event_jButton19ActionPerformed

    private void jButton20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton20ActionPerformed
        // TODO add your handling code here:
         this.setChoose(4);
         Loadban_Button();
         jTextBanSo.setText("B4");
    }//GEN-LAST:event_jButton20ActionPerformed

    private void jButton21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton21ActionPerformed
        // TODO add your handling code here:
         this.setChoose(5);
         Loadban_Button();
         jTextBanSo.setText("B5");
    }//GEN-LAST:event_jButton21ActionPerformed

    private void jButton22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton22ActionPerformed
        // TODO add your handling code here:
        this.setChoose(6);
        Loadban_Button();
        jTextBanSo.setText("B6");
    }//GEN-LAST:event_jButton22ActionPerformed

    private void jButton23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton23ActionPerformed
        // TODO add your handling code here:
        this.setChoose(7);
        Loadban_Button();
        jTextBanSo.setText("B7");
    }//GEN-LAST:event_jButton23ActionPerformed

    private void jTablechonmonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTablechonmonMouseClicked
        // TODO add your handling code here:
        monan=new MonAn();
        String chuoi = jTablechonmon.getValueAt(jTablechonmon.getSelectedRow(),0).toString(); 
        monan = sanphamdao.find(chuoi);
        if(monan.getTinhtrang().equals("no"))
        Tablechonmon.removeRow(jTablechonmon.getSelectedRow());
    }//GEN-LAST:event_jTablechonmonMouseClicked

    private void jTableBanMonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableBanMonMouseClicked
        loadtableBanMon();   

    }//GEN-LAST:event_jTableBanMonMouseClicked
    
    private void jBtnBoQuaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnBoQuaActionPerformed

        while (jTablechonmon.getRowCount() > 0) {
            ((DefaultTableModel) jTablechonmon.getModel()).removeRow(0);
        } 
        ktTrung.removeAll(ktTrung);
        jTextTongTien.setText(0+"");
        jTextBanSo.setText("Ban ");
        jTablechonmon.repaint();
        choose=0;
    }//GEN-LAST:event_jBtnBoQuaActionPerformed

    private void jBtnGopBanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnGopBanActionPerformed
        // TODO add your handling code here:
        JFrameGopBan n = new JFrameGopBan();
        n.setVisible(true);
        n.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
       loadbackroundButton();// no k chay tat d
    }//GEN-LAST:event_jBtnGopBanActionPerformed
    /////////////////////////////////////////////
        public void loadtableBanMon(){
       monan=new MonAn();     
       if(choose!=0){
        String chuoi = jTableBanMon.getValueAt(jTableBanMon.getSelectedRow(),0).toString();   
        monan = sanphamdao.find(chuoi);
        if(!ktTrung.contains(monan.getId()) )
        {
            for(int i=0;i<jTablechonmon.getRowCount();i++){
           
            }
             monan.setSoluong(Integer.parseInt((String) jComboBoxSoLuong.getSelectedItem())); 
            Tablechonmon.addRow(new Object[]{monan.getId(),monan.getTenmonan(),monan.getSoluong(),monan.getGia()});
            ktTrung.add(monan.getId());
        } 
        jTextTongTien.setText("VND ");
        jTablechonmon.setModel(Tablechonmon);
        jTablechonmon.repaint();
        jTablechonmon.revalidate();
       }
}
    ///////////////////////////////////////////////////
     ArrayList<Integer> ktTrung = new ArrayList<Integer>(); 
     List<MonAn> mon = sanphamdao.finAll();
     List dsmon;
    public void Loadban_Button(){
        /////////////////////////////load ktTrung base len
         ktTrung.removeAll(ktTrung);
        while (jTablechonmon.getRowCount() > 0) {
                    ((DefaultTableModel) jTablechonmon.getModel()).removeRow(0);
                } 
        String querry="select ma.id, ma.tenmonan,ct.soluong,ma.gia from Hoadon hd,ChitietHoadon ct,MonAn ma where hd.tongtien=0 and hd.idBan= "+choose+" and hd.id=ct.idhoadon and ma.id= ct.idmon";
        dsmon = sanphamdao.query(querry); 
       Object[] cur ;
         //Tablechonmon.addRow(new Object[]{monan.getId(),monan.getTenmonan(),monan.getSoluong(),monan.getGia()});
        for(int i=0;i<dsmon.size();i++)
        {
            cur = (Object[]) dsmon.get(i);
             Tablechonmon.addRow(new Object[]{cur[0],cur[1],cur[2],cur[3]});
             ktTrung.add((int)cur[0]);
        }
        
        //jTextTongTien.setText(dsban.get(this.getChoose()).tinhTien()+""); 
        jTablechonmon.setModel(Tablechonmon);
        jTablechonmon.revalidate();
}

    ///////////////////////////////////////////////////////
    public void Tinhtientheoban(){
        /////////////////////////////load ktTrung base len
        banan=new BanAn();
        List<MonAn> listTienmon = new ArrayList<>();
        if(choose!=0){
        String querry="select ma.id, ma.tenmonan,ct.soluong,ma.gia from Hoadon hd,ChitietHoadon ct,MonAn ma where hd.tongtien = 0 and hd.idBan= "+choose+" and hd.id=ct.idhoadon and ma.id= ct.idmon";
        dsmon = sanphamdao.query(querry); 
        Object[] cur ;
        listTienmon.clear();
        for(int i=0;i<dsmon.size();i++)
        {
            cur = (Object[]) dsmon.get(i);
            MonAn monan=new MonAn();
             monan.setSoluong(Integer.parseInt(cur[2].toString()));
             monan.setGia(Float.parseFloat(cur[3].toString()));
             listTienmon.add(i,monan); 
        }
            //listTienmon k add them...chi lay csdl len
        banan.setListmon(listTienmon);
        dsban.put(this.getChoose(),banan);
        jTextTongTien.setText(dsban.get(this.getChoose()).tinhTien()+""); 
      
        }
        
        
}
    ///////////////////////////////////////////////////////
    List idhoadon; 
    int idhoadon_NewCapNhat;
    ChitietHoadon cthdsave=new ChitietHoadon();
    private void jBtnCapNhatBanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnCapNhatBanActionPerformed
        if(choose!=0 && Tablechonmon.getRowCount()!=0){
            banan=new BanAn();
            hoadon=new Hoadon();
            cthd = new ChitietHoadon();
            int flag=1;
        String query="select hd.idBan from Hoadon hd where hd.tongtien= "+0;
        idhoadon = sanphamdao.query(query); 
        for(int i=0;i<idhoadon.size();i++)
        {
             if(choose==(int)idhoadon.get(i)){
                flag=0;
            } 
        }
        
        if(flag==1){// chon ban trong moi cap nhat trang thai
            hoadon.setIdBan(this.getChoose());
            hoadon.setNgaythang(new Date(System.currentTimeMillis())); 
            hoadon.setTongtien((float)0);
            sanphamdao.saveorupdate(hoadon);  
            banan.setId(choose);
            banan.setSonguoitoida(10);
            banan.setTinhtrang("no");
            banandao.saveorupdate(banan);
            
        }
            List queryDelCthd;
            String  cur;
            ////////////////////////////////////////////////////////
            String querry="select hd.id from Hoadon hd where hd.tongtien=0 and hd.idBan= "+choose;
            idhoadon = sanphamdao.query(querry);
            idhoadon_NewCapNhat=(int)idhoadon.get(0);//xoa nhung cthd co idhoadon nay
            String queryCthd="select cthd.id from ChitietHoadon cthd where cthd.idhoadon= "+idhoadon_NewCapNhat;
            queryDelCthd=  sanphamdao.query(queryCthd);
            for(int i=0;i<queryDelCthd.size();i++)
            {   
               // cur=queryDelCthd.get(i).toString();
                int  cur1=(int)queryDelCthd.get(i);
                cthd.setId((int)cur1);
                sanphamdao.delete(cthd); 
                //sanphamdao.delete(sanphamdao.findCthd(cur));
            }
            //////////////////////////////////////////////////////////////////
            
            for(int i=0;i<Tablechonmon.getRowCount();i++)
            {
                
                int a=(int)Tablechonmon.getValueAt(i,0);
                String b=(String)Tablechonmon.getValueAt(i,1);
                int c=(int)Tablechonmon.getValueAt(i,2);
                float d=(float)Tablechonmon.getValueAt(i,3);
                if(ktTrung.contains(a))
                {
                cthdsave.setTenmonan(b);
                cthdsave.setSoluong(c);
                cthdsave.setGia(d);
                cthdsave.setNgaythang(new Date(System.currentTimeMillis())); 
                cthdsave.setGhichu(null);
                //
                cthdsave.setIdban(choose); 
                cthdsave.setIdmon(a);    
                cthdsave.setIdhoadon(idhoadon_NewCapNhat); 
                sanphamdao.saveorupdate(cthdsave);
            }
            }

        }
        Tinhtientheoban();
       loadbackroundButton();
    }//GEN-LAST:event_jBtnCapNhatBanActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        banan=new BanAn();
        hoadon=new Hoadon();
        Tinhtientheoban();
         if(dsban.get(this.getChoose()).tinhTien()!=0){
            String querryHd="select hd.id from Hoadon hd where hd.tongtien=0 and hd.idBan= "+choose;
            idhoadon = sanphamdao.query(querryHd);
            idhoadon_NewTinhTien=(int)idhoadon.get(0);
            hoadon.setId(idhoadon_NewTinhTien);
            //sanphamdao.delete(hoadon);
            hoadon.setIdBan(choose);
            hoadon.setNgaythang(new Date(System.currentTimeMillis()));
            hoadon.setTongtien(dsban.get(this.getChoose()).tinhTien());
            sanphamdao.saveorupdateHoadon(hoadon);
            banan.setId(choose);
            banan.setSonguoitoida(10);
            banan.setTinhtrang("yes");
            banandao.saveorupdate(banan);
            chooseTT=choose;
            while (jTablechonmon.getRowCount() > 0) {
                    ((DefaultTableModel) jTablechonmon.getModel()).removeRow(0);
                } 
        }
        
        loadbackroundButton();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jComboBoxSoLuongActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxSoLuongActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxSoLuongActionPerformed

    private void jComboBoxSoLuongMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboBoxSoLuongMouseClicked
        // TODO add your handling code here:

    }//GEN-LAST:event_jComboBoxSoLuongMouseClicked

    private void jComboBoxSoLuongMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboBoxSoLuongMousePressed
        // TODO add your handling code here
      
    }//GEN-LAST:event_jComboBoxSoLuongMousePressed

    private void jBtnCapNhatBanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBtnCapNhatBanMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jBtnCapNhatBanMouseClicked

    private void jBtnchuyenbanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnchuyenbanActionPerformed
        // TODO add your handling code here:
        JFrameChuyenBan n = new JFrameChuyenBan();
        n.setVisible(true);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        loadbackroundButton();
    }//GEN-LAST:event_jBtnchuyenbanActionPerformed

    private void jBtnCapNhatSlActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnCapNhatSlActionPerformed
        // TODO add your handling code here:
        int a;    
        try{
            int idmon=Integer.parseInt(jTextIDmon.getText().toString());
            int sl=Integer.parseInt(jComboBoxSoLuong.getSelectedItem().toString());
            for(int i=0;i<Tablechonmon.getRowCount();i++){
            a = (int)Tablechonmon.getValueAt(i,0);
            if(a==idmon)
             Tablechonmon.setValueAt(sl, i, 2);
            //System.out.print(TableBan);
            }
            jTablechonmon.setModel(Tablechonmon);
            jTablechonmon.revalidate();
            
            //jComboBoxSoLuong.setSelectedIndex(0);
            }catch(Exception e){
                e.printStackTrace();
            }
    }//GEN-LAST:event_jBtnCapNhatSlActionPerformed

    private void jBtnInhoadonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnInhoadonActionPerformed
        List<Hoadon> listHoadon = new ArrayList<>();
        List<ChitietHoadon> listCthd = new ArrayList<>();
        hoadon=new Hoadon();
        if(choose!=chooseTT){
            JOptionPane.showMessageDialog(null,"Chon ban da thanh toan de In");
            return;
        }
        String queryKT_HD="select hd.tongtien FROM Hoadon hd WHERE hd.idBan= "+choose;
                 List KT_HD=sanphamdao.query(queryKT_HD);
                 Float KT=(float)KT_HD.get(0);
                 if(KT!=0)
                 {
               hoadon.setId(idhoadon_NewTinhTien);
               //spd.delete(hoadon);
               Object []dscthd1,dscthd2;
               String queryCthd="select cthd.id, cthd.tenmonan , cthd.soluong,cthd.gia, cthd.ngaythang,cthd.ghichu,cthd.idmon,cthd.idban,cthd.idhoadon FROM ChitietHoadon cthd WHERE cthd.idhoadon  = "+idhoadon_NewTinhTien;
               List id_delCthd1=sanphamdao.query(queryCthd);
                      
              // ArrayList<Integer> dstrung = new ArrayList<Integer>();
               for(int i=0;i<id_delCthd1.size();i++)
               {   
                    dscthd1=(Object [])id_delCthd1.get(i); 
                    ChitietHoadon cthd = new ChitietHoadon();
                    cthd.setId((int)dscthd1[0]);
                    cthd.setTenmonan(dscthd1[1].toString()) ;
                    cthd.setSoluong((int)dscthd1[2]);
                    cthd.setGia((float)dscthd1[3]);
                    cthd.setNgaythang((Date)dscthd1[4]);
                   // cthd.setGhichu(dscthd1[5].toString());
                    cthd.setIdmon((int)dscthd1[6]);
                    cthd.setIdban((int)dscthd1[7]);
                    cthd.setIdhoadon((int)dscthd1[8]);
                    listCthd.add(cthd);               
               }
               
         String queryIN_HD="select hd.id, hd.ngaythang, hd.tongtien,hd.idBan FROM Hoadon hd WHERE hd.id= "+idhoadon_NewTinhTien;
                List InHoadon =sanphamdao.query(queryIN_HD);
        for(int i=0;i<InHoadon.size();i++)
               {   
                    dscthd1=(Object [])InHoadon.get(i);  
                    Hoadon  inHoadon = new Hoadon();
                    inHoadon.setId((int)dscthd1[0]);
                    //inHoadon.setNgaythang((java.sql.Date)dscthd1[1]);
                    inHoadon.setTongtien((float)dscthd1[2]);
                    inHoadon.setIdBan((int)dscthd1[3]) ;
                    listHoadon.add(inHoadon);
               }    
        try {
            Inhoadon in=new Inhoadon();
            in.ghifileHD(chooseTT,idhoadon_NewTinhTien,listHoadon,listCthd); 
        } catch (IOException ex) {
            Logger.getLogger(JlFrameQuanLyBan.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
                 else{
                 JOptionPane.showMessageDialog(null,"Thanh Toan Truoc Khi IN");
                 }
    }//GEN-LAST:event_jBtnInhoadonActionPerformed

    private void jBtnchuyenbanMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jBtnchuyenbanMouseExited
        // TODO add your handling code here:
        
    }//GEN-LAST:event_jBtnchuyenbanMouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnBoQua;
    private javax.swing.JButton jBtnCapNhatBan;
    private javax.swing.JButton jBtnCapNhatSl;
    private javax.swing.JButton jBtnGopBan;
    private javax.swing.JButton jBtnInhoadon;
    private javax.swing.JButton jBtnchuyenban;
    private javax.swing.JButton jButton17;
    private javax.swing.JButton jButton18;
    private javax.swing.JButton jButton19;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton20;
    private javax.swing.JButton jButton21;
    private javax.swing.JButton jButton22;
    private javax.swing.JButton jButton23;
    private javax.swing.JButton jButton24;
    private javax.swing.JButton jButton25;
    private javax.swing.JComboBox jComboBoxSoLuong;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTableBanMon;
    private javax.swing.JTable jTablechonmon;
    private javax.swing.JTextField jTextBanSo;
    private javax.swing.JTextField jTextIDmon;
    private javax.swing.JTextField jTextTongTien;
    // End of variables declaration//GEN-END:variables
}
