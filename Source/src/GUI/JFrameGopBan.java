/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import dao.QuanlybanDao;
import dao.SanPhamDao;
import entity.BanAn;
import entity.ChitietHoadon;
import entity.Hoadon;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Tamkhung
 */
public class JFrameGopBan extends javax.swing.JFrame {
    QuanlybanDao banandao=new QuanlybanDao();
    ChitietHoadon cthd=new ChitietHoadon();
    Hoadon hoadon=new Hoadon();
    SanPhamDao spd=new SanPhamDao();
    BanAn banan=new BanAn();
    ArrayList<ChitietHoadon> listCthd;
    ArrayList<ChitietHoadon> listCthd1;
    /**
     * Creates new form JFrameGopBanAn
     */
    public JFrameGopBan() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jTextGopBan = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocation(new java.awt.Point(410, 150));

        jLabel1.setText("Gộp Vào");

        jLabel2.setText("              Chọn Bàn");

        jTextGopBan.setText("1");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9" }));

        jButton1.setText("Gộp");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Bỏ Qua");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(97, 97, 97)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jTextGopBan, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1)
                        .addGap(30, 30, 30)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(219, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextGopBan, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(103, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
             listCthd = new ArrayList<>();
        listCthd1 = new ArrayList<>();
       int a=Integer.parseInt(jTextGopBan.getText().toString());
       int b= jComboBox1.getSelectedIndex()+1;  
       //JlFrameQuanLyBan qlb=new JlFrameQuanLyBan();
        boolean check=false;
       if(a<0  || a>9)
           return; 
       BanAn banan = this.banandao.find(a);
       BanAn banan1 = this.banandao.find(b); 
       if(banan.getTinhtrang().equals("no") &&banan1.getTinhtrang().equals("no") && a!= b){
            check=true;
       }

        if(check==true){
               String queryHoadon="select hd.id FROM Hoadon hd WHERE hd.tongtien=0 and idBan  = "+a;
               List id_delHD1=spd.query(queryHoadon);
               queryHoadon="select hd.id FROM Hoadon hd WHERE hd.tongtien=0 and idBan  = "+b;
               List id_delHD2=spd.query(queryHoadon);
               if(id_delHD1 == null || id_delHD1.isEmpty())
                   System.out.println("id_delHD1 null");
               hoadon.setId((int)id_delHD1.get(0));
               spd.delete(hoadon);
               banan.setId(a);
               banan.setSonguoitoida(10);
               banan.setTinhtrang("yes");
               banandao.saveorupdate(banan);
               Object []dscthd1,dscthd2;
               String queryCthd="select cthd.id, cthd.tenmonan , cthd.soluong,cthd.gia, cthd.ngaythang,cthd.ghichu,cthd.idmon,cthd.idban,cthd.idhoadon FROM ChitietHoadon cthd WHERE cthd.idhoadon  = "+(int)id_delHD1.get(0);
               List id_delCthd1=spd.query(queryCthd);
                      queryCthd="select cthd.id, cthd.tenmonan , cthd.soluong,cthd.gia, cthd.ngaythang,cthd.ghichu,cthd.idmon,cthd.idban,cthd.idhoadon  FROM ChitietHoadon cthd WHERE cthd.idhoadon  = "+(int)id_delHD2.get(0);
               List id_delCthd2=spd.query(queryCthd);
              // ArrayList<Integer> dstrung = new ArrayList<Integer>();
               for(int i=0;i<id_delCthd1.size();i++)
               {   
                    dscthd1=(Object [])id_delCthd1.get(i); 
                    ChitietHoadon cthd = new ChitietHoadon();
                    cthd.setId((int)dscthd1[0]);
                    cthd.setTenmonan(dscthd1[1].toString()) ;
                    cthd.setSoluong((int)dscthd1[2]);
                    cthd.setGia((float)dscthd1[3]);
                    cthd.setNgaythang((Date)dscthd1[4]);
                   // cthd.setGhichu(dscthd1[5].toString());
                    cthd.setIdmon((int)dscthd1[6]);
                    cthd.setIdban((int)dscthd1[7]);
                    cthd.setIdhoadon((int)dscthd1[8]);
                    listCthd.add(cthd);
                     
                    //xoa ban 1     
                    spd.delete(cthd); 
               }
               for(int i=0;i<id_delCthd2.size();i++)
               {   
                    dscthd2=(Object [])id_delCthd2.get(i);
                    ChitietHoadon cthd = new ChitietHoadon();
                    cthd.setId((int)dscthd2[0]);
                    cthd.setTenmonan(dscthd2[1].toString()) ;
                    cthd.setSoluong((int)dscthd2[2]);
                    cthd.setGia((float)dscthd2[3]);
                    cthd.setNgaythang((Date)dscthd2[4]);
                    //cthd.setGhichu(dscthd2[5].toString());
                    cthd.setIdmon((int)dscthd2[6]);
                    cthd.setIdban((int)dscthd2[7]);
                    cthd.setIdhoadon((int)dscthd2[8]);
                    listCthd1.add(cthd);
                    //xoa ban 2
                    spd.delete(cthd);
               }
               List<ChitietHoadon> temp = new ArrayList<>();
               for(int i = 0; i < listCthd1.size(); i++){
                   for(int j = 0; j < listCthd.size(); j++){
                       if(listCthd1.get(i).getIdmon() == listCthd.get(j).getIdmon()){
                           listCthd.get(j).setSoluong(listCthd.get(j).getSoluong() + listCthd1.get(i).getSoluong());
                           break;
                       }
                       if(j == listCthd.size() - 1){
                           temp.add(listCthd1.get(i));
                       }
                   }
               }
               for (ChitietHoadon cthd : temp) {   
                   cthd.setIdhoadon((int)id_delHD2.get(0)); 
                        listCthd.add(cthd);
                        
           }   
         for (ChitietHoadon rs : listCthd) {
               rs.setIdhoadon((int)id_delHD2.get(0));
               //System.out.println("id Mon: " + rs.getIdmon() + "-- "+rs.getTenmonan() + "-- Số lượng: " + rs.getSoluong()+ "IdHoadon" + rs.getIdhoadon());
               spd.saveorupdate(rs);
         }
         this.dispose();
          JlFrameQuanLyBan fr = (JlFrameQuanLyBan)JFrameMain.getInstance().currentFrame;
            fr.loadbackroundButton();
     }
        else
        JOptionPane.showMessageDialog(null, "Bàn "+a+", " +b+ " Không được trống.");
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrameGopBan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrameGopBan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrameGopBan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrameGopBan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create andjTextGopBane form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrameGopBan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField jTextGopBan;
    // End of variables declaration//GEN-END:variables
}
